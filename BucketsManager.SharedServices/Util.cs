﻿using System.IO;

namespace BucketsManager.SharedServices
{
    public static class Util
    {

        /// <summary>
        /// StreamToBytesArray
        /// </summary>
        /// <param name="responseStream"></param>
        /// <returns></returns>
        public static byte[] StreamToBytesArray(Stream responseStream)
        {
            var buffer = new byte[16 * 1024];
            using (var memoryStream = new MemoryStream())
            {
                int read;
                while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, read);
                }
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Base64Encode
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

    }
}
