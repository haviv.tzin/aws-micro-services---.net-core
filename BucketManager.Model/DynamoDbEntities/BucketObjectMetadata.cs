﻿using Amazon.DynamoDBv2.DataModel;
using Newtonsoft.Json;

namespace BucketManager.Model.DynamoDbEntities
{
    [DynamoDBTable("BucketObjectMetadata")]
    [JsonObject]
    public class BucketObjectMetadata
    {

        [DynamoDBHashKey]
        [DynamoDBProperty(AttributeName = "BucketName")]
        [JsonProperty(PropertyName = "BucketName", NullValueHandling = NullValueHandling.Ignore)]
        public string BucketName { get; set; }

        [DynamoDBRangeKey]
        [DynamoDBProperty(AttributeName = "ObjectKey")]
        [JsonProperty(PropertyName = "ObjectKey", NullValueHandling = NullValueHandling.Ignore)]
        public string ObjectKey { get; set; }

        [DynamoDBProperty(AttributeName = "Size")]
        [JsonProperty(PropertyName = "Size", NullValueHandling = NullValueHandling.Ignore)]
        public long Size { get; set; }

        [DynamoDBProperty(AttributeName = "UserAccountId")]
        [JsonProperty(PropertyName = "UserAccountId", NullValueHandling = NullValueHandling.Ignore)]
        public string UserAccountId { get; set; }
    }
}
