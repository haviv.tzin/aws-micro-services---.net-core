﻿namespace BucketManager.Model.CustomEvents
{
    public class UploadEvent
    {
        /// <summary>
        /// Base64Content
        /// </summary>
        public string Base64Content { get; set; }

        /// <summary>
        /// ObjectName
        /// </summary>
        public string ObjectName { get; set; }
    }
}
