﻿namespace BucketManager.Model.CustomEvents
{


    public enum S3BucketActionType
    {
        NoAction,
        AddS3Object,
        MoveS3Object,
        DeleteS3Object,
        BalanceBuckets
    }

    /// <summary>
    /// BucketActionEvent
    /// </summary>
    public class BucketActionEvent
    {

        public string UserAccountId { get; set; }
        public string SourceBucketName { get; set; }
        public string TargetBucketName { get; set; }
        public string S3ObjectKey { get; set; }
        public string PipelineBucketObjectKey { get; set; }
        public S3BucketActionType S3ActionType { get; set; }
    }
}
