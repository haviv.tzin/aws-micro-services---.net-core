﻿using Amazon;

namespace BucketManager.Model
{
    /// <summary>
    /// Global
    /// </summary>
    public sealed class Global
    {
        public const string S3PipelineBucketName = "managed-bucket-pipeline";

        public const string S3Bucket1Name = "managed-bucket-1";
        public const string S3Bucket2Name = "managed-bucket-2";
        public const string S3Bucket3Name = "managed-bucket-3";

        public static RegionEndpoint Region => RegionEndpoint.USEast1;
        public const string QueueUrl =
            "https://sqs.us-east-1.amazonaws.com/884578107271/BucketManagerRequestsQueue";

        public const string QueueServiceUrl =
            "https://sqs.us-east-1.amazonaws.com";

    }
}

