using Amazon.Lambda.TestUtilities;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;
using Newtonsoft.Json;
using Xunit;

namespace BucketManager.ServiceRequestQueue.Tests
{
    public class RequestsQueueFunctionTest
    {
        [Fact]
        public void QueueBucketServiceRequest_SendSqsMessage_SentOk()
        {

            var logger = new TestLambdaLogger();

            var context = new TestLambdaContext
            {
                Logger = logger
            };

            var bucketEvent = new BucketActionEvent
            {
                S3ActionType = S3BucketActionType.MoveS3Object,
                S3ObjectKey = "0000d2cf-d3e0-400e-8626-c8b692aad4c5_test62.txt",
                SourceBucketName = Global.S3Bucket1Name,
                TargetBucketName = Global.S3Bucket2Name,
                UserAccountId = "haviv.zinn@gmail.com"
            };

            var requestsQueueFunction = new RequestsQueueFunction();
            var result = requestsQueueFunction.QueueBucketServiceRequest(bucketEvent, context);

            Assert.NotNull(result);
        }
    }
}
