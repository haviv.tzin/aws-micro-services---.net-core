﻿using System;
using System.IO;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace BucketsManager.WebApp.Util
{
    public static class AwsHelper
    {
        /// <summary>
        /// LambdaCall
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="payload"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static T LambdaCall<T>(string functionName,string payload, IConfiguration configuration) where T:class
        {

            var awsAccessKeyId = configuration.GetSection("AwsLambdaAccessKeyID").Value;
            var awsSecreteAccessKey = configuration.GetSection("AwsLambdaSecreteAccessKey").Value;

            var amazonLambdaClient = new AmazonLambdaClient(awsAccessKeyId, awsSecreteAccessKey, Global.Region);


            var lambadaInvokeRequest = new InvokeRequest
            {
                FunctionName = functionName,
                InvocationType = InvocationType.RequestResponse,
                Payload = payload
            };

            var response = amazonLambdaClient.InvokeAsync(lambadaInvokeRequest).Result;

            var streamReader = new StreamReader(response.Payload);
            JsonReader reader = new JsonTextReader(streamReader);

            var serializer = new JsonSerializer();
            var responseObject = serializer.Deserialize(reader);

            if (responseObject is string && typeof(T)!=typeof(string))
            {
               var retVal = JsonConvert.DeserializeObject<T>((string)responseObject);
               return retVal;
            }

            return (T)responseObject;
        }

        /// <summary>
        /// Upload File To AWS S3 Bucket
        /// </summary>
        /// <param name="userAccountId"></param>
        /// <param name="bucketName"></param>
        /// <param name="fileBytes"></param>
        /// <param name="fileRecord"></param>
        /// <param name="configuration"></param>
        internal static void UploadFile(string userAccountId, string bucketName, byte[] fileBytes,IFormFile fileRecord, IConfiguration configuration)
        {
            var uploadEvent = new UploadEvent
            {
                Base64Content = System.Convert.ToBase64String(fileBytes),
                ObjectName = fileRecord.FileName
            };


            var payload = JsonConvert.SerializeObject(uploadEvent);

            var objectKey = LambdaCall<string>("UploadObjectToPipelineBucket", payload, configuration);

            if (string.IsNullOrWhiteSpace(objectKey))
            {
                throw new Exception($"UploadObjectToPipelineBucket AWS service return null result. [Object:{fileRecord.FileName}]");
            }

            var bucketEvent = new BucketActionEvent
            {
                S3ActionType = S3BucketActionType.AddS3Object,
                PipelineBucketObjectKey = objectKey,
                TargetBucketName = bucketName,
                UserAccountId = userAccountId
            };

             payload = JsonConvert.SerializeObject(bucketEvent).ToString();

            var queueMsgId = LambdaCall<string>("QueueBucketManagerTask", payload, configuration);

            if (string.IsNullOrWhiteSpace(queueMsgId))
            {
                throw new Exception($"QueueBucketManagerTask AWS service return null result. [Object:{objectKey}]");
            }

        }

        internal static void BalanceUserContentBuckets(string userAccountId, IConfiguration configuration)
        {
            var bucketEvent = new BucketActionEvent
            {
                S3ActionType = S3BucketActionType.BalanceBuckets,
                UserAccountId = userAccountId
            };

            var payload = JsonConvert.SerializeObject(bucketEvent).ToString();

            var balanced = LambdaCall<string>("ManageBuckets", payload, configuration);

            if (!bool.TryParse(balanced,out var hasBalanced) || !hasBalanced)
            {
                throw new Exception("Buckets content balance process failed, see AWS CloudWatch log for more details.");
            }

        }
    }
}
