﻿namespace BucketsManager.WebApp.Model
{
    /// <summary>
    /// FilesUpload
    /// </summary>
    public class FilesUpload
    {
        public string BucketName { get; set; }
        public string UserAccountId { get; set; }

    }
}
