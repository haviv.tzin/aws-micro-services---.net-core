import React, { Component } from 'react';
import './UserEmail.css';
import {UserEmailErrors} from './UserEmailErrors'
import { AllBuckets } from './AllBuckets';

export class UserEmail extends Component {

    constructor (props) {
        super(props);
        this.state = {
          email: '',
          formErrors: {email: ''},
          emailValid: false,
          formValid: false,
          emailSubmitted: false
        }

        this.showBuckets = this.showBuckets.bind(this);
      }
    
      handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
                      () => { this.validateField(name, value) });
      }
    
      validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
    
        switch(fieldName) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            fieldValidationErrors.email = emailValid ? '' : ' is invalid';
            break;
          default:
            break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                      }, this.validateForm);
      }
    
      validateForm() {
        this.setState({formValid: this.state.emailValid });
      }
    
      errorClass(error) {
        return(error.length === 0 ? '' : 'has-error');
      }
    
      showBuckets(){
        this.setState({emailSubmitted:true});
      }

      render () {
         
        if(this.state.emailSubmitted){
            return <AllBuckets userEmail={this.state.email}></AllBuckets>
        }
        
        return(
          <form className="demoForm">
            <h2>User Email</h2>
            <div className="panel panel-default">
              <UserEmailErrors formErrors={this.state.formErrors} />
            </div>
            <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
              <label htmlFor="email">Email address</label>
              <input type="email" required className="form-control" name="email"
                placeholder="Email"
                value={this.state.email}
                onChange={this.handleUserInput}  />
            </div>
            <button type="button" className="btn btn-primary" disabled={!this.state.formValid} onClick={this.showBuckets}>Manage My AWS S3 Buckets</button>
          </form>
        )
      }
    }
    