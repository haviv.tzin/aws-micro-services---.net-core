import React, { Component } from 'react'
import {DragAndDrop} from './DragAndDrop'
import axios from 'axios';
import {ToastsContainer, ToastsStore} from 'react-toasts';

export class FileList extends Component {

    constructor(props){
        super(props);

        this.state = {
            fileNames: [],
            files:[]            
          };
          this.onNewProcessStatus = props.onNewProcessStatus;
          this.uploadFiles = this.uploadFiles.bind(this);
          this.processStatusChange = this.processStatusChange.bind(this);
    }

 processStatusChange(active){
    if(this.onNewProcessStatus instanceof Function){
        this.onNewProcessStatus(this.props.bucketName,active);
    }
 }

 handleDrop = (files) => {
    let fileNamesList = this.state.fileNames;
    let validFiles = this.state.files;

    for (var i = 0; i < files.length; i++) {
      if (!files[i].name) return;
      fileNamesList.push(files[i].name);
      validFiles.push(files[i]);
    }
    this.setState({fileNames: fileNamesList,files:validFiles})
  }

  uploadFiles() {

    if(this.state.files.length === 0) {
        ToastsStore.success( `No files to upload for '${this.props.bucketName}' bucket`);
        return;
    }
    this.processStatusChange(true);
    var data = new FormData();
    data.append('userAccountId',this.props.userAccountId);
    data.append('bucketName',this.props.bucketName);

        for(var x = 0; x < this.state.files.length; x++) {
            data.append('file' + x, this.state.files[x]);    
        }


    axios.post('api/BucketsManager/S3Upload', data)
            .then((response) =>  {
                this.processStatusChange(false);
                var responseOk = (response.ok || (response.status>199 && response.status<300));
                if(responseOk){
                    ToastsStore.success( `All files uploaded to S3 filtering bucket and will deliver to '${this.props.bucketName}' bucket in few seconds`);
                    this.setState({
                        fileNames: [],
                        files:[]
                    });
                    return;            
                }
                if(response.status===404)
                {
                    setTimeout(this.uploadFiles,1000);
                    return;
                }    
                console.log(response);
                ToastsStore.error("Upload files to S3 buckets process completed with error, see console for more details.");            

            }).catch(error => {
                this.processStatusChange(false);
                if(error.message === "Network Error"){
                    setTimeout(this.uploadFiles,1000);
                    return;
                }
                console.log(error);
                ToastsStore.error("Upload files to S3 buckets process completed with error, see console for more details.");
                this.setState({
                    fileNames: [],
                    files:[]
                });
             }); 

}

render() {
    let i=0;


    return (
        <div>
            <ToastsContainer store={ToastsStore}/>
      <DragAndDrop handleDrop={this.handleDrop}>
        <div style={{height: 300, width: 250}}>
          {this.state.fileNames.map((file) =>{
              i++;
           return <div key={i}>{file}</div>
          }
          )}
        </div>
      </DragAndDrop>
      </div>      
    )
  }
}
