import React, { Component } from 'react';
import './Bucket.css';
import {FileList} from './FileList'

export class Bucket extends Component {
  static displayName = Bucket.name;

  constructor (props) {
    super(props);
    this.state = { currentSize: 0 ,percent:0};
    this.sizeChanged = this.sizeChanged.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.filesListRef = {};
    this.onNewProcessStatus = props.onNewProcessStatus;
    this.processStatusChange = this.processStatusChange.bind(this);    
  }

  processStatusChange(processId,active){
    if(this.onNewProcessStatus instanceof Function){
        this.onNewProcessStatus(processId,active);
    }
 }

  sizeChanged() {
    var percent = 100*parseInt(this.props.size,10)/parseInt(this.props.maxSize,10);
    var deg = 360*percent/100 ;
    var element = this.refs.progress.getDOMNode();
    element.style.transform = 'rotate(-'+ deg +'deg)';
    this.setState({
      currentSize: this.props.size,
      percent: percent
    });
  }  


   classSet ( classes ) {
    return typeof classes !== 'object' ?
      Array.prototype.join.call(arguments, ' ') :
      Object.keys(classes).filter(function(className){
        return classes[className];
      }).join(' ');
  }
  
   uploadFiles(){
      this.filesListRef.uploadFiles();
    }

  render () {
    var kbSize = this.props.size/1000;

    var classes = this.classSet({
      "progress-pie-chart": true,
      "gt-50": this.state.percent > 33
    });

 
    return (
      <div>
      <label className="bucketLabel">AWS S3 Bucket: {this.props.name}</label>
      <div className="progress clearfix">            
        <div className={classes}>
          <div className="ppc-progress">
            <div className="ppc-progress-fill" ref="progress"></div>
          </div>
          <div className="ppc-percents">
            <div className="pcc-percents-wrapper">
              <span>{kbSize + 'kb'}</span>
            </div>
          </div>
        </div>
      </div>
      <FileList userAccountId={this.props.userAccountId} bucketName={this.props.name} ref={instance => { this.filesListRef = instance; }} onNewProcessStatus={this.processStatusChange}></FileList>
      </div>

    );
  }
}
