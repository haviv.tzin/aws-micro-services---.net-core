import React, { Component  } from 'react';
import { Bucket } from './Bucket';
import './UserEmail.css';
import {ToastsContainer, ToastsStore} from 'react-toasts';


export class AllBuckets extends Component{
    static displayName = AllBuckets.name;
    
    constructor (props) {
        super(props);

        this.state = { bucketSizes: props.bucketSizes?JSON.parse(props.bucketSizes):[0,0,0], isProcessRunning:false };
        this.sizeChanged = this.sizeChanged.bind(this);
        this.uploadFiles = this.uploadFiles.bind(this);
        this.newProcessStatus = this.newProcessStatus.bind(this);
        this.refreshBucketsContentSize = this.refreshBucketsContentSize.bind(this);
        this.balanceBucketsContent = this.balanceBucketsContent.bind(this);
        this.bucket1Ref = {};
        this.bucket2Ref = {};
        this.bucket3Ref = {};
        this.runningProcessCount = 0;
        this.refreshBucketsContentSize();
    }
        
    newProcessStatus(processId,active){
        let prevCount = Math.max(0, this.runningProcessCount);
        active?this.runningProcessCount++:Math.max(0,this.runningProcessCount--);
        let statusChanged = prevCount===0 || this.runningProcessCount===0;
        if(statusChanged){
            this.setState({ isProcessRunning: this.runningProcessCount>0});
        }
    }

    balanceBucketsContent(){
        this.newProcessStatus('balanceBucketsContent',true);
        ToastsStore.info("Job triggered by user. Balancing buckets content, this process may take a minute or two...");

        fetch(`api/BucketsManager/BalanceUserContentBuckets?userAccountId=${this.props.userEmail}`)
        .then(response => {            
            this.newProcessStatus('balanceBucketsContent',false);
            var responseOk = (response.ok || (response.status>199 && response.status<300));
            if(responseOk){
                ToastsStore.success("Buckets content balance process completed.");
                this.refreshBucketsContentSize();
                return;
            }
            if(response.status===404)
            {
                setTimeout(this.balanceBucketsContent,1000);
                return;
            }
            console.log(response);
            ToastsStore.error("Buckets content balance process completed with error, see console for more details.");            
        },
        (error) => {
            this.newProcessStatus('balanceBucketsContent',false);
            if (error.message ==="Failed to fetch"){
                setTimeout(this.balanceBucketsContent,1000);
                return;
            }
            ToastsStore.error("Buckets content balance process completed with error, see console for more details.");       
        });

    }

    refreshBucketsContentSize(){

        let hasError = false;
        this.newProcessStatus('refreshBucketsContentSize',true);
        ToastsStore.info("Retriving buckets current content size...");

            fetch(`api/BucketsManager/GetBucketSizes?userAccountId=${this.props.userEmail}`)
            .then(response => {
                var responseOk = (response.ok || (response.status>199 && response.status<300));
                if(responseOk){
                    return response.json();
                }
                hasError = true;
                if(response.status===404)
                {
                    this.newProcessStatus('refreshBucketsContentSize',false);
                        setTimeout(this.refreshBucketsContentSize,1000);
                        return;
                }
                console.log(response);
                ToastsStore.error("Refresh Buckets Content Size process completed with error, see console for more details.");            
                return [0,0,0];
            },
            (error) => {
                hasError = true;
                this.newProcessStatus('refreshBucketsContentSize',false);
                if (error.message ==="Failed to fetch"){
                    setTimeout(this.refreshBucketsContentSize,1000);
                    return;
                }
                ToastsStore.error("Refresh Buckets Content Size process completed with error, see console for more details.");    
            })
            .then(data => {
                if(hasError)
                {
                    return;
                }
                this.newProcessStatus('refreshBucketsContentSize',false);
                ToastsStore.success("Buckets current content size is up to date.");
                this.setState({ bucketSizes: data});    
            });

    }

    componentDidCatch(error, info) {
        ToastsStore.error(error);
        console.log(error);
        console.log(info);
        this.runningProcessCount=0;
        this.setState({ isProcessRunning: false});

      }
          
      sizeChanged() {
        this.setState({
            bucketSizes: this.props.bucketSizes
        });
      }        

      uploadFiles(){
        ToastsStore.info('files upload process is running...');
        this.bucket1Ref.uploadFiles();
        this.bucket2Ref.uploadFiles();
        this.bucket3Ref.uploadFiles();
    }

      render(){
          var bucketSizes = this.state.bucketSizes;
          var maxSize = bucketSizes[0]+bucketSizes[1]+bucketSizes[2];
          return <div>
              <ToastsContainer store={ToastsStore}/>
              <table>
              <tbody>
                  <tr>
                    <td>
                        <table>
                            <tbody>
                            <tr>
                            <td className="bucketTd">
                            <Bucket size={bucketSizes[0]} name="managed-bucket-1" maxSize={maxSize} userAccountId={this.props.userEmail} ref={instance => { this.bucket1Ref = instance; }} onNewProcessStatus={this.newProcessStatus}></Bucket>
                            </td>
                            <td className="bucketTd">
                            <Bucket size={bucketSizes[1]} name="managed-bucket-2" maxSize={maxSize} userAccountId={this.props.userEmail} ref={instance => { this.bucket2Ref = instance; }} onNewProcessStatus={this.newProcessStatus}></Bucket>
                            </td>
                            <td className="bucketTd">
                            <Bucket size={bucketSizes[2]} name="managed-bucket-3" maxSize={maxSize} userAccountId={this.props.userEmail} ref={instance => { this.bucket3Ref = instance; }} onNewProcessStatus={this.newProcessStatus}></Bucket>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                    </td>
                  </tr>
                  <tr>
                      <td>
<p>To upload files, drag and drop file into the target bucket. Once all files in buckets click on "Upload" button to start the upload process.</p>
<table>
    <tbody>
        <tr>
            <td>
            <button type="button" className="btn btn-primary"  onClick={this.uploadFiles} disabled={this.state.isProcessRunning}>Upload Files</button>
            </td>
            <td>
            <button type="button" className="btn btn-primary"  onClick={this.refreshBucketsContentSize} disabled={this.state.isProcessRunning}>Refresh Buckets</button>
            </td>
            <td>
            <button type="button" className="btn btn-primary"  onClick={this.balanceBucketsContent} disabled={this.state.isProcessRunning}>Balance Buckets Content(Job)</button>
            </td>
        </tr>
    </tbody>
</table>


                      </td>
                  </tr>
              </tbody>
          </table> 
          </div>

      }
}