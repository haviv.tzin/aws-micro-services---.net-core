﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BucketsManager.WebApp.Model;
using BucketsManager.WebApp.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BucketsManager.WebApp.Controllers
{
    [Route("api/[controller]")]
    public class BucketsManagerController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public BucketsManagerController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("[action]")]
        public IEnumerable<long> GetBucketSizes(string userAccountId)
        {
            var response = AwsHelper.LambdaCall< IEnumerable<long>>("ManageBuckets",
                $"{{\"userAccountId\":\"{userAccountId}\",\"serviceName\":\"GetBucketSize\"}}", _configuration);

            return response;
        }

        [HttpPost("S3Upload")]
        public async Task<IActionResult> S3Upload(FilesUpload uploadRecord)
        {
            var files = Request.Form.Files;

            if (files == null || !files.Any())
            {
                return NoContent();
            }

            var filesArray = files.ToArray();
            foreach (var file in filesArray)
            {
                if (file.Length == 0)
                {
                    continue;
                }
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();

                    if (fileBytes.Length > 0)
                    {
                        //ToDo - haviv remove try catch block
                        try
                        {
                            AwsHelper.UploadFile(uploadRecord.UserAccountId, uploadRecord.BucketName, fileBytes, file,
                                _configuration);
                        }
                        catch (Exception e)
                        {
                            //ToDo - handle this error - although error not expected. 
                            Console.WriteLine(e);
                        }
                    }
                }
            }

            return Accepted();

        }

        [HttpGet("[action]")]
        public IActionResult BalanceUserContentBuckets(string userAccountId)
        {
            AwsHelper.BalanceUserContentBuckets(userAccountId, _configuration);

            return Ok();
        }



    }

}