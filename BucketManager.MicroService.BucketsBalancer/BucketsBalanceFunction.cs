using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Amazon.S3;
using Amazon.S3.Model;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;
using BucketManager.Model.DynamoDbEntities;
using BucketsManager.SharedServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace BucketManager.BucketsBalancer
{
    public class BucketsBalanceFunction
    {


        /// <summary>
        /// Default constructor. This constructor is used by Lambda to construct the instance. When invoked in a Lambda environment
        /// the AWS credentials will come from the IAM role associated with the function and the AWS region will be set to the
        /// region the Lambda function is executed in.
        /// </summary>
        public BucketsBalanceFunction()
        {
            S3Client = new AmazonS3Client(Global.Region);
        }

        /// <summary>
        /// Constructs an instance with a preconfigured S3 client. This can be used for testing the outside of the Lambda environment.
        /// </summary>
        /// <param name="s3Client"></param>
        public BucketsBalanceFunction(IAmazonS3 s3Client)
        {
            S3Client = s3Client;
        }

        /// <summary>
        /// S3Client
        /// </summary>
        private IAmazonS3 S3Client { get; }

        /// <summary>
        /// BucketsTaskHandler
        /// </summary>
        /// <param name="serviceRequest"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string BucketsTaskHandler(JObject serviceRequest, ILambdaContext context)
        {
            var sqsEvent = serviceRequest.ToObject<SQSEvent>();
            if(sqsEvent.Records!=null)
            {
                HandleSqsEvent(sqsEvent, context);
                return true.ToString();
            }

            //BucketActionEvent

            var bucketActionEvent = serviceRequest.ToObject<BucketActionEvent>();
            if (bucketActionEvent != null && bucketActionEvent.S3ActionType!=S3BucketActionType.NoAction)
            {
                bool processOk;
                try
                {
                    processOk = ExecuteBucketAction(bucketActionEvent, context);
                }
                catch (Exception e)
                {
                    processOk = false;
                    context.Logger.LogLine($"Error while executing '{bucketActionEvent.S3ActionType}' Buckets task");
                    context.Logger.LogLine(e.Message);
                    context.Logger.LogLine(e.StackTrace);
                }

                return processOk.ToString();
            }

            var userAccountId = serviceRequest["userAccountId"].ToString();
            var serviceName = serviceRequest["serviceName"].ToString();

            switch (serviceName)
            {
                case "GetBucketSize":
                    var retVal = GetBucketSizes(userAccountId);
                    return JsonConvert.SerializeObject(retVal);

                default:
                    return true.ToString();
            }

        }

        /// <summary>
        /// BucketsTaskHandler
        /// </summary>
        /// <param name="sqsEvent"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private void HandleSqsEvent(SQSEvent sqsEvent, ILambdaContext context)
        {

            var failureList = new List<BucketActionEvent>();

            foreach (var message in sqsEvent.Records)
            {
                var bucketActionMessage = JsonConvert.DeserializeObject<BucketActionEvent>(message.Body);

                bool processOk;
                try
                {
                    processOk = ExecuteBucketAction( bucketActionMessage, context);
                }
                catch (Exception e)
                {
                    processOk = false;
                    context.Logger.LogLine($"Error while executing '{bucketActionMessage.S3ActionType}' Buckets task");
                    context.Logger.LogLine(e.Message);
                    context.Logger.LogLine(e.StackTrace);
                    context.Logger.LogLine($"Buckets Task: {message.Body}");
                }

                if (!processOk)
                {
                    failureList.Add(bucketActionMessage);
                }
            }

            //return failureList;
        }

        /// <summary>
        /// ExecuteBucketAction
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bucketActionMessage"></param>
        /// <returns></returns>
        private bool ExecuteBucketAction(BucketActionEvent bucketActionMessage, ILambdaContext context)
        {
            bool processOk = true;

            switch (bucketActionMessage.S3ActionType)
            {
                case S3BucketActionType.AddS3Object:
                    processOk = BucketsTaskAddObject(bucketActionMessage, context);
                    break;
                case S3BucketActionType.MoveS3Object:
                    processOk = BucketsTaskMoveObject(bucketActionMessage, context);
                    break;
                case S3BucketActionType.DeleteS3Object:
                    processOk = BucketsTaskDeleteObject(bucketActionMessage, context);
                    break;
                case S3BucketActionType.BalanceBuckets:
                    processOk = BucketsTaskBalanceBuckets(bucketActionMessage.UserAccountId, context);
                    break;
            }

            return processOk;
        }


        /// <summary>
        /// BucketsTaskAddObject
        /// </summary>
        /// <param name="bucketActionMessage"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool BucketsTaskAddObject(BucketActionEvent bucketActionMessage, ILambdaContext context)
        {
            var passPreliminaryCheck = FilePreliminaryCheck(bucketActionMessage.PipelineBucketObjectKey);
            if (!passPreliminaryCheck)
            {
                return false;
            }
            bucketActionMessage.S3ActionType = S3BucketActionType.MoveS3Object;
            bucketActionMessage.SourceBucketName = Global.S3PipelineBucketName;
            bucketActionMessage.S3ObjectKey = bucketActionMessage.PipelineBucketObjectKey;
            var retVal = BucketsTaskMoveObject(bucketActionMessage, context);

            return retVal;

        }

        /// <summary>
        /// File Preliminary Check: map, categories, virus Check etc.
        ///  Not Implemented - add your own Implementation here
        /// </summary>
        /// <param name="pipelineBucketObjectKey"></param>
        /// <returns></returns>
        private static bool FilePreliminaryCheck(string pipelineBucketObjectKey)
        {
            // Not Implemented

            return true;
        }

        /// <summary>
        /// UpdateObjectMetadata
        /// </summary>
        /// <param name="bucketObjectMetadata"></param>
        private async Task<bool> UpdateObjectMetadata(BucketObjectMetadata bucketObjectMetadata)
        {
            using (var dynamoContext = new DynamoDBContext(new AmazonDynamoDBClient(Global.Region)))
            {
                var existRecords = dynamoContext.ScanAsync<BucketObjectMetadata>(new[]
                {
                    new ScanCondition("ObjectKey", ScanOperator.Equal, new object[] {bucketObjectMetadata.ObjectKey}),
                }).GetRemainingAsync().Result;

                if (existRecords != null && existRecords.Any())
                {
                    var record = existRecords.First();
                    await dynamoContext.DeleteAsync<BucketObjectMetadata>(record.BucketName, record.ObjectKey);
                }

                if (!string.IsNullOrWhiteSpace(bucketObjectMetadata.BucketName))
                {
                    await dynamoContext.SaveAsync(bucketObjectMetadata);
                }
            }

            return true;
        }

        /// <summary>
        /// BucketsTaskMoveObject
        /// </summary>
        /// <param name="bucketActionMessage"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool BucketsTaskMoveObject(BucketActionEvent bucketActionMessage, ILambdaContext context)
        {
            GetObjectResponse getObjectResponse;
            long objectSize;
            try
            {
                getObjectResponse = S3Client
                    .GetObjectAsync(bucketActionMessage.SourceBucketName, bucketActionMessage.S3ObjectKey).Result;
                if (getObjectResponse.HttpStatusCode != HttpStatusCode.OK)
                {
                    context.Logger.Log(
                        $"S3 Get Object Request return HTTP Status:'{getObjectResponse.HttpStatusCode}'.");
                    return false;
                }

                var request = new ListObjectsV2Request
                {
                    BucketName = bucketActionMessage.SourceBucketName,
                    Prefix = bucketActionMessage.S3ObjectKey
                };
                var response = S3Client.ListObjectsV2Async(request).Result;
                objectSize = response.S3Objects.Any() ? response.S3Objects.First().Size : 0;
            }
            catch (Exception e)
            {
                context.Logger.LogLine(
                    $"Error getting object {bucketActionMessage.S3ObjectKey} from bucket {bucketActionMessage.SourceBucketName}. Make sure they exist and your bucket is in the same region as this function.");
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.StackTrace);
                throw;
            }

            var objBytes = Util.StreamToBytesArray(getObjectResponse.ResponseStream);
            var putRequest = new PutObjectRequest
            {
                BucketName = bucketActionMessage.TargetBucketName,
                Key = bucketActionMessage.S3ObjectKey,
                InputStream = new MemoryStream(objBytes),
            };

            try
            {
                var putObjectResponse = S3Client.PutObjectAsync(putRequest).Result;
                if (putObjectResponse.HttpStatusCode != HttpStatusCode.OK)
                {
                    context.Logger.Log(
                        $"S3 Put Object Request return HTTP Status:'{putObjectResponse.HttpStatusCode}'.");
                    return false;
                }
            }
            catch (Exception e)
            {
                context.Logger.LogLine(
                    $"Error putting object {putRequest.Key} from bucket {putRequest.BucketName}. Make sure they exist and your bucket is in the same region as this function.");
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.StackTrace);
                throw;
            }

            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                    {BucketName = bucketActionMessage.SourceBucketName, Key = bucketActionMessage.S3ObjectKey};
                var deleteObjectResponse = S3Client.DeleteObjectAsync(deleteObjectRequest).Result;
                if (deleteObjectResponse.HttpStatusCode != HttpStatusCode.OK)
                {
                    context.Logger.Log(
                        $"S3 Delete Object Request return HTTP Status:'{deleteObjectResponse.HttpStatusCode}'.");
                }
            }
            catch (Exception e)
            {
                context.Logger.LogLine(
                    $"Error delete object {bucketActionMessage.S3ObjectKey} from bucket {bucketActionMessage.SourceBucketName}. Make sure they exist and your bucket is in the same region as this function.");
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.StackTrace);
            }

            var bucketObjectMetadata = new BucketObjectMetadata
            {
                BucketName = bucketActionMessage.TargetBucketName,
                ObjectKey = bucketActionMessage.S3ObjectKey,
                Size = objectSize,
                UserAccountId = bucketActionMessage.UserAccountId
            };

            var retVal = UpdateObjectMetadata(bucketObjectMetadata).Result;

            return retVal;
        }

        /// <summary>
        /// BucketsTaskDeleteObject
        /// </summary>
        /// <param name="bucketActionMessage"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool BucketsTaskDeleteObject(BucketActionEvent bucketActionMessage, ILambdaContext context)
        {
            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                    {BucketName = bucketActionMessage.TargetBucketName, Key = bucketActionMessage.S3ObjectKey};
                var deleteObjectResponse = S3Client.DeleteObjectAsync(deleteObjectRequest).Result;
                if (deleteObjectResponse.HttpStatusCode != HttpStatusCode.OK)
                {
                    context.Logger.Log(
                        $"S3 Delete Object Request return HTTP Status:'{deleteObjectResponse.HttpStatusCode}'.");
                    return false;
                }
            }
            catch (Exception e)
            {
                context.Logger.LogLine(
                    $"Error delete object {bucketActionMessage.S3ObjectKey} from bucket {bucketActionMessage.TargetBucketName}. Make sure they exist and your bucket is in the same region as this function.");
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.StackTrace);
            }

            var bucketObjectMetadata = new BucketObjectMetadata
            {
                ObjectKey = bucketActionMessage.S3ObjectKey,
                UserAccountId = bucketActionMessage.UserAccountId
            };

            var retVal = UpdateObjectMetadata(bucketObjectMetadata).Result;
            return retVal;
        }

        /// <summary>
        /// BucketsTaskBalanceBuckets
        /// </summary>
        /// <param name="userAccountId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private bool BucketsTaskBalanceBuckets(string userAccountId, ILambdaContext context)
        {
            context.Logger.LogLine($"Processing BalanceBuckets request...");
            RunBucketsBalancer(context,userAccountId,3);
            context.Logger.LogLine($"BalanceBuckets request process completed.");
            return true;
        }

        /// <summary>
        /// GetAllBucketsMetadata
        /// </summary>
        /// <param name="userAccountId"></param>
        /// <returns></returns>
        private static List<BucketObjectMetadata> GetAllBucketsMetadata(string userAccountId)
        {
            using (var dynamoContext = new DynamoDBContext(new AmazonDynamoDBClient(Global.Region)))
            {
                var allBuckets = dynamoContext.ScanAsync<BucketObjectMetadata>(new[]
                {
                    new ScanCondition("UserAccountId", ScanOperator.Equal, userAccountId),
                }).GetRemainingAsync().Result;

                return allBuckets;
            }
        }

        /// <summary>
        /// GetBucketSizes
        /// </summary>
        /// <param name="userAccountId"></param>
        /// <returns></returns>
        private long[] GetBucketSizes(string userAccountId)
        {
            var retVal = new long[3];
            var bucketsMeta = GetAllBucketsMetadata(userAccountId);
            retVal[0] = bucketsMeta.Where(m => m.BucketName == Global.S3Bucket1Name).Sum(b => b.Size);
            retVal[1] = bucketsMeta.Where(m => m.BucketName == Global.S3Bucket2Name).Sum(b => b.Size);
            retVal[2] = bucketsMeta.Where(m => m.BucketName == Global.S3Bucket3Name).Sum(b => b.Size);

            return retVal;
        }

        /// <summary>
        /// RunBucketsBalancer
        /// </summary>
        /// <param name="context"></param>
        /// <param name="userAccountId"></param>
        /// <param name="repeatCount"></param>
        private void RunBucketsBalancer(ILambdaContext context, string userAccountId, int repeatCount)
        {
            if (repeatCount == 0)
            {
                return;
            }
            repeatCount--;

            var allBuckets = GetAllBucketsMetadata(userAccountId);


            var bucket1 = allBuckets.Where(b => b.BucketName == Global.S3Bucket1Name).ToList();
            var bucket1BusySize = bucket1.Sum(b => b.Size);

            var bucket2 = allBuckets.Where(b => b.BucketName == Global.S3Bucket2Name).ToList();
            var bucket2BusySize = bucket2.Sum(b => b.Size);

            var bucket3 = allBuckets.Where(b => b.BucketName == Global.S3Bucket3Name).ToList();
            var bucket3BusySize = bucket3.Sum(b => b.Size);

            var delta12 = Math.Abs(bucket1BusySize- bucket2BusySize);
            var delta13 = Math.Abs(bucket1BusySize - bucket3BusySize);
            var delta23 = Math.Abs(bucket2BusySize - bucket3BusySize);

            var twoBucketsMaxDelta = new List<long> { delta12, delta13, delta23 }.Max();

            List<BucketObjectMetadata> oneBucket;
            List<BucketObjectMetadata> secondBucket;

            string oneBucketName;
            string secondBucketName;

            if (delta12 == twoBucketsMaxDelta)
            {
                oneBucket = bucket1;
                secondBucket = bucket2;
                oneBucketName = Global.S3Bucket1Name;
                secondBucketName = Global.S3Bucket2Name;
            }
            else if (delta13 == twoBucketsMaxDelta)
            {
                oneBucket = bucket1;
                secondBucket = bucket3;
                oneBucketName = Global.S3Bucket1Name;
                secondBucketName = Global.S3Bucket3Name;
            }
            else
            {
                oneBucket = bucket2;
                secondBucket = bucket3;
                oneBucketName = Global.S3Bucket2Name;
                secondBucketName = Global.S3Bucket3Name;
            }

            var actions = BalanceTwoBuckets(oneBucket, oneBucketName,  secondBucket, secondBucketName);

            if (actions == null || !actions.Any())
            {
                return;
            }

            actions.ForEach(a=>ExecuteBucketAction(a,context));

            RunBucketsBalancer(context, userAccountId, repeatCount);
        }

        /// <summary>
        /// BalanceTwoBuckets
        /// </summary>
        /// <param name="bucket1"></param>
        /// <param name="bucket1Name"></param>
        /// <param name="bucket2"></param>
        /// <param name="bucket2Name"></param>
        /// <returns></returns>
        private static List<BucketActionEvent> BalanceTwoBuckets( List<BucketObjectMetadata> bucket1,string bucket1Name,  List<BucketObjectMetadata> bucket2, string bucket2Name)
        {
            var bucket1BusySize = bucket1.Sum(a => a.Size);
            var bucket2BusySize = bucket2.Sum(a => a.Size);

            var sourceBucket = bucket1BusySize > bucket2BusySize ? bucket1 : bucket2;
            var sourceBucketName = bucket1BusySize > bucket2BusySize ? bucket1Name : bucket2Name;

            var targetBucketName = bucket1BusySize > bucket2BusySize ? bucket2Name : bucket1Name;

            var targetSum = Math.Abs(bucket1BusySize - bucket2BusySize) / 2;
            var sourceSizeSet = sourceBucket.Select(b => b.Size).ToList();

            var subset=new List<long>();
            FindObjectsSubsetToMoveToTargetBucket(sourceSizeSet, targetSum, subset);

            var subsetGroups = subset.GroupBy(a => a);

            var objectsTransferList = new List<BucketObjectMetadata>();
            foreach (var subsetGroup in subsetGroups)
            {
                var showsCount = subsetGroup.Count();
                objectsTransferList.AddRange(sourceBucket.Where(a=>a.Size==subsetGroup.Key).Take(showsCount));
            }

            var retVal = new List<BucketActionEvent>();
            objectsTransferList.ForEach(objMetadata =>
                {
                    retVal.Add(new BucketActionEvent
                    {
                        S3ActionType = S3BucketActionType.MoveS3Object, SourceBucketName = sourceBucketName,
                        TargetBucketName = targetBucketName, S3ObjectKey = objMetadata.ObjectKey,
                        UserAccountId = objMetadata.UserAccountId
                    });
                });

            return retVal;
        }

        /// <summary>
        /// FindObjectsSubsetToMoveToTargetBucket
        /// </summary>
        /// <param name="sourceSizeSet"></param>
        /// <param name="targetSum"></param>
        /// <param name="subset"></param>
        /// <returns></returns>
        private static void FindObjectsSubsetToMoveToTargetBucket(List<long> sourceSizeSet, long targetSum, List<long> subset)
        {
            var relevant = sourceSizeSet.Where(b => targetSum - b >= 0).ToList();
            if (relevant.Count == 0)
            {
                return;
            }

            var delta = relevant.Min(a => targetSum - a);
            var newSize = targetSum - delta;
            sourceSizeSet.Remove(newSize);
            subset.Add(newSize);

            if (delta == 0)
            {
                return;
            }

            FindObjectsSubsetToMoveToTargetBucket(sourceSizeSet, delta, subset);
        }
    }
}
