using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.S3;
using Amazon.S3.Model;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace BucketManager.S3Uploader
{
    public class S3ObjectUploadFunction
    {

        /// <summary>
        /// Deafult Ctor
        /// </summary>
        public S3ObjectUploadFunction()
        {
            S3Client = new AmazonS3Client(Global.Region);
        }

        /// <summary>
        /// Constructs an instance with a preconfigured S3 client. This can be used for testing the outside of the Lambda environment.
        /// </summary>
        /// <param name="s3Client"></param>
        public S3ObjectUploadFunction(IAmazonS3 s3Client)
        {
            S3Client = s3Client;
        }

        /// <summary>
        /// S3Client
        /// </summary>
        private IAmazonS3 S3Client { get; set; }


        /// <summary>
        /// UploadToPipelineBucket
        /// </summary>
        /// <param name="s3UploadEvent"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<string> UploadToPipelineBucket(UploadEvent s3UploadEvent, ILambdaContext context)
        {
            if (s3UploadEvent == null || string.IsNullOrWhiteSpace(s3UploadEvent.Base64Content))
            {
                context.Logger.Log($"Upload event data not available");
                return null;
            }

            try
            {

                var key = $"{Guid.NewGuid()}_{s3UploadEvent.ObjectName}";

                var bytesContent = Convert.FromBase64String(s3UploadEvent.Base64Content);

                // 1. Put object-specify only key name for the new object.
                var putRequest1 = new PutObjectRequest
                {
                    BucketName = Global.S3PipelineBucketName,
                    Key = key,
                    InputStream = new MemoryStream(bytesContent)
                };

                var response1 = await S3Client.PutObjectAsync(putRequest1);

                if (response1.HttpStatusCode == HttpStatusCode.OK)
                {
                    return key;
                }

                context.Logger.Log(
                    $"S3 Put Object Request return HTTP Status:'{response1.HttpStatusCode}'.");
                return null;


            }

            catch (AmazonS3Exception e)
            {
                context.Logger.Log(
                    $"Error encountered ***. Message:'{e.Message}' when writing an object");
                return null;
            }
            catch (Exception e)
            {
                context.Logger.Log(
                    $"Unknown encountered on server. Message:'{e.Message}' when writing an object");
                return null;
            }
        }

    }
}
