using Amazon.Lambda.TestUtilities;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace BucketManager.BucketsBalancer.Tests
{
    public class BucketsBalanceFunctionTest
    {


        [Fact]
        public void BucketsTaskHandler_GetBucketSizes_ReturnLongArray()
        {

            var logger = new TestLambdaLogger();

            var context = new TestLambdaContext
            {
                Logger = logger
            };


            var function = new BucketsBalanceFunction();


            var jsonData = JObject.Parse($"{{\"userAccountId\":\"{TestHelper.TestAccountId}\",\"serviceName\":\"GetBucketSize\"}}");
            var jsonString = function.BucketsTaskHandler( jsonData, context);

            var result = JsonConvert.DeserializeObject<long[]>(jsonString);

            Assert.Equal(3, result.Length);
        }

        /// <summary>
        /// BucketsTaskHandler_AddObject_ExistInBucket
        /// </summary>
        [Fact]
        public void BucketsTaskHandler_AddObject_ExistInBucket()
        {

            var logger = new TestLambdaLogger();

            var context = new TestLambdaContext
            {
                Logger = logger
            };

            var objectKey = TestHelper.UploadRandomObjectToBucket("test.txt", context);

            var bucketActionEvent = new BucketActionEvent
            {
                S3ActionType = S3BucketActionType.AddS3Object,
                PipelineBucketObjectKey = objectKey,
                TargetBucketName = Global.S3Bucket1Name,
                UserAccountId = TestHelper.TestAccountId
            };



            var function = new BucketsBalanceFunction();

            var result = function.BucketsTaskHandler(JObject.FromObject(bucketActionEvent),context);

            Assert.Equal("True",result);
        }


        /// <summary>
        /// BucketsTaskHandler_BalanceTheBuckets_Balanced
        /// </summary>
        [Fact]
        public void BucketsTaskHandler_BalanceTheBuckets_Balanced()
        {

            var logger = new TestLambdaLogger();

            var context = new TestLambdaContext
            {
                Logger = logger
            };

            TestHelper.FillBucketsRandomly(14,context);

            var bucketActionEvent = new BucketActionEvent
            {
                S3ActionType = S3BucketActionType.BalanceBuckets,
                UserAccountId = TestHelper.TestAccountId
            };


            var function = new BucketsBalanceFunction();

            var result = function.BucketsTaskHandler(JObject.FromObject(bucketActionEvent), context);

            Assert.Equal("True", result);

            var getBucketSizeEvent = JObject.Parse(
                $"{{\"userAccountId\":\"{TestHelper.TestAccountId}\",\"serviceName\":\"GetBucketSize\"}}");

            result = function.BucketsTaskHandler(getBucketSizeEvent, context);
            var bucketSizes = JsonConvert.DeserializeObject<long[]>(result);

            var ratio1 = (double) bucketSizes[0] /(double) bucketSizes[1];
            var ratio2 = (double)bucketSizes[0] / (double)bucketSizes[2];

            var bucketsBalanced = (ratio1 > 0.74 || ratio1 < 1.34) && (ratio2 > 0.74 || ratio2 < 1.34);

            Assert.True(bucketsBalanced);
        }

        [Fact]
        public void BucketsTaskHandler_GetBucketContentSizes_ReturnLongArray()
        {
            var logger = new TestLambdaLogger();

            var context = new TestLambdaContext
            {
                Logger = logger
            };
            var function = new BucketsBalanceFunction();

            var getBucketSizeEvent = JObject.Parse(
                $"{{\"userAccountId\":\"{TestHelper.TestAccountId}\",\"serviceName\":\"GetBucketSize\"}}");

            var result = function.BucketsTaskHandler(getBucketSizeEvent, context);
            var bucketSizes = JsonConvert.DeserializeObject<long[]>(result);

            Assert.Equal(3, bucketSizes.Length);


        }

    }
}
