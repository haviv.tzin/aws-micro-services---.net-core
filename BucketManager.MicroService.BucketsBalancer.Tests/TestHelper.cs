﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.Lambda.SQSEvents;
using Amazon.Lambda.TestUtilities;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;
using BucketManager.S3Uploader;
using BucketsManager.SharedServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BucketManager.BucketsBalancer.Tests
{
    internal static class TestHelper
    {
        private static readonly Random Random = new Random();
        public const string TestAccountId = "haviv.zinn@gmail.com";


        /// <summary>
        /// Fill Buckets Randomly
        /// </summary>
        public static void FillBucketsRandomly(int objectsCount, TestLambdaContext context)
        {
            var bucketsBalanceFunction = new BucketsBalanceFunction();
            var s3ObjectUploadFunction = new S3ObjectUploadFunction();

            var switchBucketIndex1 = Random.Next(1, objectsCount/2);
            var switchBucketIndex2 = Random.Next(switchBucketIndex1+1, objectsCount);
            for (var i = 0; i < objectsCount; i++)
            {
                // STEP 1: Upload object to Pipeline Bucket
                //-------------------------------------------
                var content = RandomString(Random.Next(10, 250));
                var uploadEvent = new UploadEvent
                { ObjectName = $"test{i}.txt", Base64Content = Util.Base64Encode(content) };

                var objectKey = s3ObjectUploadFunction.UploadToPipelineBucket(uploadEvent, context).Result;

                if (string.IsNullOrWhiteSpace(objectKey))
                {
                    continue;
                }

                // STEP 2: Add new object request to SQS queue
                //---------------------------------------------
                var targetBucketName = i > switchBucketIndex1 ? Global.S3Bucket1Name : Global.S3Bucket2Name;
                targetBucketName = i > switchBucketIndex2 ? Global.S3Bucket3Name : targetBucketName;

                var bucketEvent = new BucketActionEvent
                {
                    S3ActionType = S3BucketActionType.AddS3Object,
                    PipelineBucketObjectKey = objectKey,
                    TargetBucketName = targetBucketName,
                    UserAccountId = TestAccountId
                };


                bucketsBalanceFunction.BucketsTaskHandler(JObject.FromObject(bucketEvent), context);

            }
        }

        /// <summary>
        /// UploadRandomObjectToBucket
        /// </summary>
        /// <param name="objectName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string UploadRandomObjectToBucket(string objectName, TestLambdaContext context)
        {
            var s3ObjectUploadFunction = new S3ObjectUploadFunction();

            var content = RandomString(Random.Next(10, 250));
            var uploadEvent = new UploadEvent
                { ObjectName = objectName, Base64Content = Util.Base64Encode(content) };

            var objectKey = s3ObjectUploadFunction.UploadToPipelineBucket(uploadEvent, context).Result;

            return objectKey;
        }

        /// <summary>
        /// RandomString
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

    }
}
