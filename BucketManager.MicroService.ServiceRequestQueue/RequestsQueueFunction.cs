using System.Net;
using Amazon.Lambda.Core;
using Amazon.SQS;
using Amazon.SQS.Model;
using BucketManager.Model;
using BucketManager.Model.CustomEvents;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace BucketManager.ServiceRequestQueue
{
    public class RequestsQueueFunction
    {

        /// <summary>
        /// SqsClient
        /// </summary>
        private AmazonSQSClient SqsClient { get; } = new AmazonSQSClient(new AmazonSQSConfig { ServiceURL = Global.QueueServiceUrl });

        /// <summary>
        /// QueueBucketServiceRequest
        /// </summary>
        /// <param name="bucketEvent"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string QueueBucketServiceRequest(BucketActionEvent bucketEvent, ILambdaContext context)
        {
            if (bucketEvent == null)
            {
                context.Logger.Log($"BucketActionEvent is null, skip queue process.");
                return null;
            }

            var isValidEventData = ValidateEvent(bucketEvent, context);
            if (!isValidEventData)
            {
                return null;
            }

            var message = JsonConvert.SerializeObject(bucketEvent);

            var sendMessageRequest = new SendMessageRequest {QueueUrl = Global.QueueUrl, MessageBody = message};

            var result = SqsClient.SendMessageAsync(sendMessageRequest).Result;

            if (result.HttpStatusCode != HttpStatusCode.OK)
            {
                context.Logger.Log($"SQS message enqueue return HTTP status {result.HttpStatusCode}.");
                return null;
            }

            return result.MessageId;
        }


        /// <summary>
        /// Validate BucketActionEvent event
        /// </summary>
        /// <param name="bucketEvent"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private static bool ValidateEvent(BucketActionEvent bucketEvent, ILambdaContext context)
        {
            if (bucketEvent == null)
            {
                context.Logger.Log($"BucketActionEvent is null, skip queue process.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(bucketEvent.UserAccountId))
            {
                context.Logger.Log($"BucketActionEvent does not contain UserAccountId, abort '{bucketEvent.S3ActionType}'.");
                return false;
            }

            var hasSourceBucket = !string.IsNullOrWhiteSpace(bucketEvent.SourceBucketName);
            var hasTargetBucket = !string.IsNullOrWhiteSpace(bucketEvent.TargetBucketName);
            var hasObjectKey = !string.IsNullOrWhiteSpace(bucketEvent.S3ObjectKey);

            string eventStr;
            switch (bucketEvent.S3ActionType)
            {
                case S3BucketActionType.AddS3Object:
                    if (string.IsNullOrWhiteSpace(bucketEvent.PipelineBucketObjectKey))
                    {
                        context.Logger.Log($"BucketActionEvent does not contain PipelineBucketObjectKey value, abort '{bucketEvent.S3ActionType}'.");
                        eventStr = JsonConvert.SerializeObject(bucketEvent);
                        context.Logger.Log(eventStr);
                        return false;
                    }

                    if (!hasTargetBucket)
                    {
                        context.Logger.Log($"BucketActionEvent does not contain target bucket name, abort '{bucketEvent.S3ActionType}'.");
                        eventStr = JsonConvert.SerializeObject(bucketEvent);
                        context.Logger.Log(eventStr);
                        return false;
                    }
                    break;

                case S3BucketActionType.MoveS3Object:
                   if (!hasObjectKey)
                    {
                        context.Logger.Log($"BucketActionEvent does not contain target bucket object key value, abort '{bucketEvent.S3ActionType}'.");
                        eventStr = JsonConvert.SerializeObject(bucketEvent);
                        context.Logger.Log(eventStr);
                        return false;
                    }
                   if (!hasTargetBucket)
                   {
                       context.Logger.Log($"BucketActionEvent does not contain target bucket name, abort '{bucketEvent.S3ActionType}'.");
                       eventStr = JsonConvert.SerializeObject(bucketEvent);
                       context.Logger.Log(eventStr);
                       return false;
                   }

                   if (!hasSourceBucket)
                   {
                       context.Logger.Log($"BucketActionEvent does not contain source bucket name, abort '{bucketEvent.S3ActionType}'.");
                       eventStr = JsonConvert.SerializeObject(bucketEvent);
                       context.Logger.Log(eventStr);
                       return false;
                   }
                    break;

                case S3BucketActionType.DeleteS3Object:
                    if (!hasObjectKey)
                    {
                        context.Logger.Log($"BucketActionEvent does not contain target bucket object key value, abort '{bucketEvent.S3ActionType}'.");
                        eventStr = JsonConvert.SerializeObject(bucketEvent);
                        context.Logger.Log(eventStr);
                        return false;
                    }
                    if (!hasTargetBucket)
                    {
                        context.Logger.Log($"BucketActionEvent does not contain target bucket name, abort '{bucketEvent.S3ActionType}'.");
                        eventStr = JsonConvert.SerializeObject(bucketEvent);
                        context.Logger.Log(eventStr);
                        return false;
                    }

                    break;
            }

            return true;
        }
    }
}
