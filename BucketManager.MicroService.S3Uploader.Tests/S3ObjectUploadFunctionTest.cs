using System.Threading.Tasks;
using Amazon.Lambda.TestUtilities;
using BucketManager.Model.CustomEvents;
using BucketsManager.SharedServices;
using Newtonsoft.Json;
using Xunit;

namespace BucketManager.S3Uploader.Tests
{
    public class S3ObjectUploadFunctionTest
    {
        [Fact]
        public async Task UploadToPipelineBucket_UploadFile_ReturnS3ObjectKey()
        {

            var logger = new TestLambdaLogger();

            var context = new TestLambdaContext
            {
                Logger = logger
            };

            var s3ObjectUploadFunction = new S3ObjectUploadFunction();
            var uploadEvent = new UploadEvent
                {ObjectName = "unitTest.txt", Base64Content = Util.Base64Encode("Hello World")};

            var objectKey = s3ObjectUploadFunction.UploadToPipelineBucket(uploadEvent, context).Result;

            Assert.NotNull(objectKey);
        }

    }
}
