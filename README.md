# AWS MicroServices (.Net Core)

This is demo project show an implementation of AWS cloud services which can be combine into a work flow of organization process.
# Features

*   .Net Core (C#)
	-	MicroService
	-	Web Application
	-	Unit Tests (xUnit)	
*   AWS Services:
    -   [AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)
    -   [Amazon SQS](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/welcome.html)
    -   [Amazon S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/Welcome.html)
    -   [Amazon DynamoDB  (NoSql)](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html)
    -   [Amazon EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
    -   [Amazon ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)
    -   [AWS Elastic Beanstalk](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html)
    -   [AWS IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
    -   [AWS X-Ray](https://docs.aws.amazon.com/xray/latest/devguide/aws-xray.html)
    -   [AWS CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html)
*   UI
	- [React JS](https://reactjs.org)

# Product Requirements
For this exercises  I have come up with this user story which is good for the educational purpose only.
The organization  maintain 3 files repositories in the cloud which let the employees store all the business documents and media files.

However as a preliminary check the file has to certify,recorded in the database and pass a virus check. For this purpose the file has to be stored in temporary files repository until its clear to be moved to the target files repository.

One of the organization concern is to avoid overload on certain files repository.
In order to prevent overload on files repository a content balancer background job is require.

The last requirement is to build web interface for:
* Uploading files by the users (employees)
* Review the user files content size on each repository.
* (For Admin)Trigger the content balancer job manually.

# Architecture
 Components

![Components diagram](https://docs.google.com/uc?id=1AaFsFH-IURACRtzLA1Ll-8eMaH3Y2Gz3)

* AWS S3 Buckets:
	- Pipeline bucket - temporary files repository where the preliminary check takes place.
	- Manage buckets - organization files repositories.
* "File Upload" Micro Service - AWS Lambda service that handle file upload request. It stores the file in the Pipeline bucket and provides key to the file.

* "File Upload Request Queue" Micro Service - AWS Lambda service that queue a request for file preliminary check on a file store in the Pipeline bucket. Once the request adds to the queue (AWS SQS) the queue fire event which handle by "Buckets Balancer" Micro Service.

* "Buckets Balancer" Micro Service - AWS Lambda service handle SQS event source and calls from other AWS services. This is buckets managing service that support the following requests:
	- Add Object To Bucket
	- Move Object From One Bucket To Other.
	- Delete Object From Bucket
	- Balance Buckets Content (greedy algorithm)
The service maintain file meta data NoSql repository (DynamoDB) for quick search and reference base on attributes.

* [Web Application (UI)](http://bucketsmanagerwebapp-test.us-east-1.elasticbeanstalk.com) - provide interface for user intend to upload files to organization files repositories. It also provide content size status for the logged user on each repository.
Assuming the user has admin rights, the user able to trigger content balancer background job manually.
This web application is implemented in .Net core and the presentation layer implemented in [React JS](https://reactjs.org)
See [Demo](http://bucketsmanagerwebapp-test.us-east-1.elasticbeanstalk.com)

# Logging
* [AWS CloudWatch](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/WhatIsCloudWatch.html).
* [AWS X-Ray](https://docs.aws.amazon.com/xray/latest/devguide/aws-xray.html).

# Testing
* xUnit - Unit Tests
* AWS Console Test Tools
* Visual Studio - AWS Explorer

# Note
In web application project (BucketsManager.WebApp) edit "appsettings.json" file and enter your:
* AwsLambdaAccessKeyID
* AwsLambdaSecreteAccessKey

# Amazon Cloud Environment Setup
* AWS Account - if you don't have one you can create the AWS Free Tier account.
* DynamoDB Table
	- Table Name: BucketObjectMetadata
	- Hash Key Name: BucketName  {Type: string}
	- Range Key Name: ObjectKey {Type: string}
* EC2 instance to contain(Beanstalk) the web application.
* 4 x Amazon S3 Buckets:
	- managed-bucket-1
	- managed-bucket-2
	- managed-bucket-3
	- managed-bucket-pipeline
* Amazon SQS queue - BucketManagerRequestsQueue
* Use IAM to create new role for the micro services (Lambda)
* Deploy 3 micro services (Lambda):
	- "File Upload" Micro Service - deploy with the name "UploadObjectToPipelineBucket".
	- "File Upload Request Queue" Micro Service - deploy with the name "QueueBucktsManagerTask"
	- "Buckets Balancer" Micro Service - deploy with the name "ManageBuckets"

![Cloud Environment Setup](https://docs.google.com/uc?id=1bffR8AEtJc-ymbOk6YcAn2STcAbXLQ3e)

